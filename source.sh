#!/bin/bash
# Clone this script in your ROM Repo using following commands.
# cd rom_repo
# curl https://gitlab.com/garlic-oreo/scripts/raw/lineage-15.1/source.sh > source.sh
# to get the device sources simply use bash source.sh
git clone https://gitlab.com/garlic-oreo/android_device_yu_garlic.git -b lineage-15.1 device/yu/garlic && git clone https://gitlab.com/garlic-oreo/android_kernel_yu_msm8937.git -b lineage-15.1 kernel/yu/msm8937 && git clone https://gitlab.com/garlic-oreo/vendor_yu_garlic.git -b lineage-15.1 vendor/yu/garlic